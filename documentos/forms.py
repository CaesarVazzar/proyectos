from django import forms

from models import *


class DocumentoForm(forms.ModelForm):
    class Meta:
        model = Documentos
        fields = ('descripcion', 'documento', 'usuario', 'departamento')
	def __init__(self, *args, **kwargs):
		super(DocumentoForm, self).__init__(*args, **kwargs)
		for field in iter(self.fields):
				self.fields[field].widget.attrs.update({
				'class': 'form-control'
			})
