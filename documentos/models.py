from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import Group

class Documentos(models.Model):
    descripcion = models.CharField(max_length=255, blank=True)
    documento = models.FileField(upload_to='documentos')
    uploaded_at = models.DateTimeField(auto_now_add=True)
    usuario = models.ForeignKey(User)
    departamento = models.ForeignKey(Group)

    def __unicode__(self):
        return self.descripcion