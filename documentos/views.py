from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest
from documentos.models import *
from documentos.forms import *
from django.shortcuts import render,redirect,get_object_or_404
from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required(login_url='/login/')
def index(request):
    """Renders the home page."""
    documento = Documentos.objects.all()
    return render(request, 'documentos/index.html', { 'documento': documento })

def instruccionesSAPP(request):
    """Renderiza el html de instrucciones."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'documentos/instruccionesSAPP.html',
    )


class instrucciones_trabajo_sapp(ListView):
    model = Documentos
    template_name = "documentos/instruccionesSAPP.html"

    def get_queryset(self):
        g = Group.objects.get(user = self.request.user)
        if g.name == "sistemas": 
            documentos = Documentos.objects.filter(departamento_id="1").values_list('usuario__pk', flat=True)
            documento = Documentos.objects.filter (usuario_id=documentos)
        if g.name == "costos": 
            documentos = Documentos.objects.filter(departamento_id="2").values_list('usuario__pk', flat=True)
            documento = Documentos.objects.filter (usuario_id=documentos)
        if g.name == "administrador":
            documento = Documentos.objects.all()
        return documento


def instruccionesVS(request):
    """Renderiza el html de instrucciones."""
    return render(
        request,
        'documentos/instruccionesVS.html',
    )

def form(request):
    if request.method == 'POST':
        form = DocumentoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('documentos:index')
    else:
        form = DocumentoForm()
    return render(request, 'documentos/subir.html', {
        'form': form
    })
