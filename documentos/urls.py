from django.conf.urls import patterns, url
from . import views
import documentos.views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',

	url(r'^$', views.index, name = 'index'),
	url(r'^instrucciones_trabajo_sapp$', views.instrucciones_trabajo_sapp.as_view(), name = 'sapp'),
	url(r'^instruccionesSAPP', views.instruccionesSAPP, name='instruccionesSAPP'),
	url(r'^instruccionesVS$', views.instruccionesVS, name='instruccionesVS'),
	url(r'^form$', views.form, name='form'),
)

