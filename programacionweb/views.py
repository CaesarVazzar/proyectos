# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest
from django.shortcuts import render,redirect,get_object_or_404
from django.shortcuts import render

# Create your views here.
def home(request):
    """Renderiza el home page de esta app."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'programacionweb/index.html',
    ) 