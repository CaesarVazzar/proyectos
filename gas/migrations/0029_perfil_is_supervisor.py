# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0028_auto_20180405_1140'),
    ]

    operations = [
        migrations.AddField(
            model_name='perfil',
            name='is_supervisor',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
    ]
