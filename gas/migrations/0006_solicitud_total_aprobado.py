# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0005_auto_20180216_2240'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitud',
            name='total_aprobado',
            field=models.DecimalField(null=True, verbose_name=b'Total Aprobado', max_digits=8, decimal_places=2, blank=True),
        ),
    ]
