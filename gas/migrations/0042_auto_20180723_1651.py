# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0041_auto_20180723_1607'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='solicitud',
            options={'permissions': (('can_review', 'Can review'), ('can_approve', 'Can approve'), ('can_pay', 'Can pay'), ('can_cancel', 'Can cancel'), ('can_kilometros', 'Can kilometros'), ('can_generadores', 'Can generadores'), ('can_saldos', 'Can saldos'))},
        ),
    ]
