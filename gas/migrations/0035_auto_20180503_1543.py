# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0034_auto_20180502_1618'),
    ]

    operations = [
        migrations.CreateModel(
            name='HorasGeneradores',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('horas_uso', models.IntegerField(null=True, blank=True)),
                ('fecha', models.DateTimeField(null=True, blank=True)),
                ('generador', models.ForeignKey(to='gas.Generadores')),
            ],
        ),
        migrations.AlterModelOptions(
            name='solicitud',
            options={'permissions': (('can_review', 'Can review'), ('can_approve', 'Can approve'), ('can_pay', 'Can pay'), ('can_cancel', 'Can cancel'), ('can_kilometros', 'Can kilometros'), ('can_generadores', 'Can generadores'))},
        ),
    ]
