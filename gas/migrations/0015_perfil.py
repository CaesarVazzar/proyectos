# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gas', '0014_vehiculo_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero_tarjeta', models.CharField(max_length=20, null=True, blank=True)),
                ('banco', models.CharField(max_length=30, null=True, blank=True)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('vehiculo', models.ForeignKey(to='gas.Kilometraje')),
            ],
        ),
    ]
