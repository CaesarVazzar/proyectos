# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0007_auto_20180221_2005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banco',
            name='numero_tarjeta',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
