# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0039_auto_20180523_1637'),
    ]

    operations = [
        migrations.AddField(
            model_name='perfil',
            name='saldo_tarjeta',
            field=models.DecimalField(null=True, max_digits=8, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='perfil',
            name='tiene_tarjeta',
            field=models.BooleanField(default=True),
        ),
    ]
