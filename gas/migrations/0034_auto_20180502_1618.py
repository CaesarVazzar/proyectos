# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0033_remove_generadores_horas_uso'),
    ]

    operations = [
        migrations.AlterField(
            model_name='generadores',
            name='marca',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
    ]
