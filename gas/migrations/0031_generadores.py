# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gas', '0030_auto_20180426_0919'),
    ]

    operations = [
        migrations.CreateModel(
            name='Generadores',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('generador', models.CharField(max_length=50, null=True, blank=True)),
                ('obra', models.CharField(max_length=200, null=True, blank=True)),
                ('estatus', models.CharField(max_length=20, null=True, blank=True)),
                ('fecha_compra', models.DateField(null=True, blank=True)),
                ('serie', models.CharField(max_length=30, null=True, blank=True)),
                ('marca', models.CharField(max_length=10, null=True, blank=True)),
                ('marcador', models.CharField(max_length=15, null=True, blank=True)),
                ('estatus_obra', models.CharField(max_length=30, null=True, blank=True)),
                ('horas_uso', models.IntegerField(null=True, blank=True)),
                ('residente', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
