# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0024_auto_20180403_0934'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='perfil',
            name='vehiculo',
        ),
        migrations.AddField(
            model_name='perfil',
            name='edad',
            field=models.CharField(max_length=3, null=True),
        ),
        migrations.AddField(
            model_name='perfil',
            name='telefono',
            field=models.IntegerField(max_length=30, null=True),
        ),
    ]
