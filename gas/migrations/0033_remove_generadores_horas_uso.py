# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0032_auto_20180502_0953'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='generadores',
            name='horas_uso',
        ),
    ]
