# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0002_auto_20180216_2054'),
    ]

    operations = [
        migrations.RenameField(
            model_name='solicitud',
            old_name='id_banco',
            new_name='numero_tarjeta',
        ),
    ]
