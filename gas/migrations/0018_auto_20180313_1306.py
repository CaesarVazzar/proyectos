# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0017_auto_20180313_1224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perfil',
            name='numero_tarjeta',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
    ]
