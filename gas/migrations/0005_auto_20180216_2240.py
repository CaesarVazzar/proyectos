# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0004_auto_20180216_2059'),
    ]

    operations = [
        migrations.RenameField(
            model_name='banco',
            old_name='total_depositado',
            new_name='saldo',
        ),
        migrations.RemoveField(
            model_name='solicitud',
            name='banco',
        ),
        migrations.AlterField(
            model_name='banco',
            name='usuario',
            field=models.ForeignKey(related_name='user', to=settings.AUTH_USER_MODEL),
        ),
    ]
