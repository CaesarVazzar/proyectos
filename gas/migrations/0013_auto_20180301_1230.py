# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0012_auto_20180301_1229'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kilometraje',
            name='vehiculo',
            field=models.ForeignKey(to='gas.Vehiculo'),
        ),
    ]
