# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0008_auto_20180221_2127'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kilometraje',
            name='fecha',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
