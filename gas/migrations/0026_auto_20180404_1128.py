# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0025_auto_20180404_0912'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perfil',
            name='zona_usuario',
            field=models.CharField(max_length=15, null=True, blank=True),
        ),
    ]
