# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0013_auto_20180301_1230'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehiculo',
            name='status',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
