# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0019_auto_20180313_1311'),
    ]

    operations = [
        migrations.AddField(
            model_name='perfil',
            name='zona_usuario',
            field=models.CharField(max_length=10, null=True, blank=True),
        ),
    ]
