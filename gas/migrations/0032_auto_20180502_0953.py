# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0031_generadores'),
    ]

    operations = [
        migrations.RenameField(
            model_name='generadores',
            old_name='residente',
            new_name='usuario',
        ),
    ]
