# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0016_auto_20180313_1222'),
    ]

    operations = [
        migrations.RenameField(
            model_name='perfil',
            old_name='vehiculo_id',
            new_name='vehiculo',
        ),
    ]
