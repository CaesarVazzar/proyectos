# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Asignacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('plantas', models.IntegerField(null=True, blank=True)),
                ('tarjeta', models.CharField(max_length=16, null=True, blank=True)),
                ('usuario', models.ForeignKey(related_name='usuario_asignacion', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Banco',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero_tarjeta', models.IntegerField(max_length=20, null=True, blank=True)),
                ('banco', models.CharField(max_length=30, null=True, blank=True)),
                ('total_depositado', models.CharField(max_length=10, null=True, blank=True)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Kilometraje',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('kilometraje', models.IntegerField(null=True, blank=True)),
                ('fecha', models.DateTimeField(auto_now_add=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Solicitud',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('estatus', models.SmallIntegerField(default=1, verbose_name=b'Estatus', choices=[(1, b'SOLICITADO'), (2, b'REVISION'), (3, b'APROBADO'), (4, b'PAGADO'), (5, b'CANCELADO')])),
                ('clave_proyecto', models.CharField(max_length=200, null=True, blank=True)),
                ('justificacion', models.TextField(verbose_name=b'Justificacion')),
                ('gasolina_troca', models.DecimalField(null=True, verbose_name=b'Gasolina troca', max_digits=8, decimal_places=2, blank=True)),
                ('gasolina_generador', models.DecimalField(null=True, verbose_name=b'Gasolina generador', max_digits=8, decimal_places=2, blank=True)),
                ('total', models.DecimalField(null=True, verbose_name=b'Total Solicitado', max_digits=8, decimal_places=2, blank=True)),
                ('fecha_solicitud', models.DateTimeField(auto_now_add=True, null=True)),
                ('fecha_revision', models.DateTimeField(auto_now_add=True, null=True)),
                ('fecha_aprobacion', models.DateTimeField(auto_now_add=True, null=True)),
                ('fecha_pago', models.DateTimeField(auto_now_add=True, null=True)),
                ('fecha_cancelacion', models.DateTimeField(auto_now_add=True, null=True)),
                ('numero_tarjeta', models.ForeignKey(to='gas.Banco')),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'permissions': (('can_review', 'Can review'), ('can_approve', 'Can approve'), ('can_pay', 'Can pay'), ('can_cancel', 'Can cancel')),
            },
        ),
        migrations.CreateModel(
            name='Vehiculo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.SmallIntegerField(default=1, verbose_name=b'Tipo', choices=[(1, b'CAMIONETA'), (2, b'AUTO'), (3, b'GENIE'), (4, b'GRUA'), (5, b'MANIPULADOR')])),
                ('marca', models.CharField(max_length=15, null=True, blank=True)),
                ('modelo', models.CharField(max_length=30, null=True, blank=True)),
                ('zona', models.CharField(max_length=15, null=True, blank=True)),
                ('descripcion', models.CharField(max_length=100, null=True, blank=True)),
                ('placa', models.CharField(max_length=15, null=True, blank=True)),
                ('obra', models.CharField(max_length=50, null=True, blank=True)),
                ('anio', models.CharField(max_length=15, null=True, blank=True)),
                ('kilometraje', models.IntegerField(null=True, blank=True)),
                ('rendimiento', models.DecimalField(null=True, max_digits=5, decimal_places=2, blank=True)),
                ('color', models.CharField(max_length=15, null=True, blank=True)),
                ('usuario', models.ForeignKey(related_name='usuario', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='kilometraje',
            name='vehiculo',
            field=models.ForeignKey(related_name='vehiculo_kilometraje', to='gas.Vehiculo'),
        ),
    ]
