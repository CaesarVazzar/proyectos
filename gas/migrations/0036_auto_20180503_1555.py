# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0035_auto_20180503_1543'),
    ]

    operations = [
        migrations.RenameField(
            model_name='generadores',
            old_name='generador',
            new_name='descripcion',
        ),
    ]
