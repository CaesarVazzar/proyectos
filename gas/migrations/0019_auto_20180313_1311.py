# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0018_auto_20180313_1306'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vehiculo',
            old_name='kilometraje_recorrido',
            new_name='kilometraje_odometro',
        ),
    ]
