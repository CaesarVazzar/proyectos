# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0042_auto_20180723_1651'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='kilometraje',
            name='activo',
        ),
    ]
