# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0021_solicitud_perfil'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='solicitud',
            name='perfil',
        ),
    ]
