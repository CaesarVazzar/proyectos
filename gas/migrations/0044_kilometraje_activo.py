# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0043_remove_kilometraje_activo'),
    ]

    operations = [
        migrations.AddField(
            model_name='kilometraje',
            name='activo',
            field=models.BooleanField(default=True),
        ),
    ]
