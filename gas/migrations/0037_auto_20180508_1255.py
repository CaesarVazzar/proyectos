# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0036_auto_20180503_1555'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vehiculo',
            name='rendimiento',
            field=models.CharField(max_length=15, null=True, blank=True),
        ),
    ]
