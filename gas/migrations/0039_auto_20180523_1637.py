# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0038_auto_20180523_1044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kilometraje',
            name='fecha',
            field=models.DateField(null=True, blank=True),
        ),
    ]
