# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0020_perfil_zona_usuario'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitud',
            name='perfil',
            field=models.ForeignKey(default=1, to='gas.Perfil'),
            preserve_default=False,
        ),
    ]
