# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0015_perfil'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='perfil',
            name='vehiculo',
        ),
        migrations.AddField(
            model_name='perfil',
            name='vehiculo_id',
            field=models.ForeignKey(default=58, to='gas.Vehiculo'),
            preserve_default=False,
        ),
    ]
