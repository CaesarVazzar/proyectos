# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0023_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='asignacion',
            name='usuario',
        ),
        migrations.RemoveField(
            model_name='banco',
            name='usuario',
        ),
        migrations.RemoveField(
            model_name='user',
            name='groups',
        ),
        migrations.RemoveField(
            model_name='user',
            name='user_permissions',
        ),
        migrations.DeleteModel(
            name='Asignacion',
        ),
        migrations.DeleteModel(
            name='Banco',
        ),
        migrations.DeleteModel(
            name='User',
        ),
    ]
