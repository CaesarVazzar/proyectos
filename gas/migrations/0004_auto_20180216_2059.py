# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0003_auto_20180216_2057'),
    ]

    operations = [
        migrations.RenameField(
            model_name='solicitud',
            old_name='numero_tarjeta',
            new_name='banco',
        ),
    ]
