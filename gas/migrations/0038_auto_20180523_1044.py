# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gas', '0037_auto_20180508_1255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kilometraje',
            name='kilometraje',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=0, blank=True),
        ),
        migrations.AlterField(
            model_name='vehiculo',
            name='rendimiento',
            field=models.DecimalField(null=True, max_digits=5, decimal_places=2, blank=True),
        ),
    ]
