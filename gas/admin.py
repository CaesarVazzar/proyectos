from django.contrib import admin
from gas.models import Solicitud,Vehiculo, Kilometraje, Generadores,Perfil

# Register your models here.

admin.site.register(Solicitud)
admin.site.register(Vehiculo)
admin.site.register(Generadores)
admin.site.register(Perfil)

# admin.site.register(Kilometraje)
