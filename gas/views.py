from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from gas.models import Solicitud, Vehiculo, Kilometraje, Perfil, Generadores, HorasGeneradores
from gas.forms import Solicitud, Vehiculo, NvaSemanaForm, KilometrajeForm
from datetime import datetime
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import Group

def home(request):
    """Renderiza el home page de esta app."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'gas/index.html',
    ) 


#funcion para cambiar el estado de todas las solicitudes a pagado
def pay_request(request):
    list = Solicitud.objects.filter(estatus = 3)
    for obj in list:
        obj.estatus = 4
        obj.fecha_pago = datetime.today()
        obj.save()
    return render(
        request,
        'gas/solicitudes.html',
    )

#funcion para cmabiar el estado de todas las solicitudes a revision
def rev_request(request):
    list = Solicitud.objects.filter(estatus = 1)
    for obj in list:
        obj.estatus = 2
        obj.fecha_revision = datetime.today()
        obj.save()
    return render(
        request,
        'gas/index.html',
    )

#funcion para cambiar el estado de todas las solicitudes a aprobado
def approval_request(request):
    list = Solicitud.objects.filter(estatus = 2)
    for obj in list:
        obj.estatus = 3
        obj.fecha_aprobacion = datetime.today()
        obj.save()
    return render(
        request,
        'gas/index.html',
    )

#funcion para mostrar exito en los post. No se usa.
def exito(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'gas/exito.html',
    )

#funcion para cambiar una sola solicitud de estatus a pagada
def pay_request_object(request,solicitud):
    obj = Solicitud.objects.get(id = solicitud)
    if obj.estatus == 3: 
        obj.estatus = 4
    obj.fecha_pago = datetime.today()
    obj.save()
    print solicitud
    return redirect('gas:solicitudes')


def cancel_request_object(request,solicitud):
    obj = Solicitud.objects.get(id = solicitud)
    if obj.estatus != 4:
        obj.estatus = 5
    obj.fecha_pago = datetime.today()
    obj.save()
    return redirect('gas:solicitudes')


def aprove_request_object(request,solicitud):
    obj = Solicitud.objects.get(id = solicitud)
    if obj.estatus == 2:
        obj.estatus = 3
    obj.fecha_pago = datetime.today()
    obj.total_aprobado = obj.total
    obj.save()
    print solicitud
    return redirect('gas:solicitudes')


def review_request_object(request,solicitud):
    obj = Solicitud.objects.get(id = solicitud)
    if obj.estatus == 1:
        obj.estatus = 2
    obj.fecha_pago = datetime.today()
    obj.save()
    return redirect('gas:solicitudes')


class PerfilView(ListView):
    model = Perfil
    template_name = 'gas/perfil.html'
    def get_queryset(self):
        perfil = Perfil.objects.filter(usuario_id=self.request.user.pk)
        # print self.request.user.pk
        return perfil

    def get_context_data(self, **kwargs):
        context = super(PerfilView, self).get_context_data(**kwargs)
        # usuario = self.kwargs.get('pk')
        print (self.kwargs.get('vehiculoPerfil'))
        context['vehiculoPerfil'] = Vehiculo.objects.filter(usuario_id=self.request.user.pk)
     
        return context

#opcion 1
#     def get_context_data(self, *args, **kwargs):
#         context =super(PerfilView,self).get_context_data(*args,**kwargs)
#         context['moreitems'] = Vehiculo.objects.order_by('marca')
#         return context
# #opcion 2
#     def get_context_data(self, **kwargs):
#         context = super(PerfilView, self).get_context_data(**kwargs)
#         context['vehiculo_by_marca'] = Vehiculo.objects.order_by('marca')[:5]
#         context['vehiculo_by_modelo'] = Vehiculo.objects.order_by('modelo')[:5]
#         return context
#en template puedes accesar a vehiculo_by_marca y vehiculo_by_modelo


class agregar_vehiculo(CreateView):
    model = Vehiculo
    fields = ['marca', 'modelo', 'zona', 'descripcion', 'placa', 'obra', 'anio','kilometraje_odometro', 'rendimiento', 'usuario', 'tipo', 'status']
    template_name = 'gas/agregar_vehiculo.html'
    success_url = reverse_lazy('gas:km_recorrer')


# class SolicitudesView(ListView):
#     model = Solicitud
#     template_name = 'gas/solicitudes.html'

#     def get_queryset(self):
#         query_set = Group.objects.filter(user = self.request.user)
#         for g in query_set:
#             if g.name == "supervisores_gas":
#                 solicitudes = Solicitud.objects.all()
#             else:
#                 solicitudes = Solicitud.objects.filter(usuario_id=self.request.user.pk)
#                 if g.name == "supervisor_mantenimiento":
#                     usuarios_ids = Perfil.objects.filter(zona_usuario__icontains="mantenimiento").values_list('usuario__pk', flat=True)
#                     solicitudes = Solicitud.objects.filter (usuario_id=usuarios_ids)
#                 else:
#                     solicitudes = Solicitud.objects.filter(usuario_id=self.request.user.pk)
#                     if g.name == "supervisor_norte":
#                         usuarios_ids = Perfil.objects.filter(zona_usuario__icontains="norte").values_list('usuario__pk', flat=True)
#                         solicitudes = Solicitud.objects.filter (usuario_id=usuarios_ids)
#                     else:
#                         solicitudes = Solicitud.objects.filter(usuario_id=self.request.user.pk)
#                         if g.name == "supervisor_juarez":
#                             usuarios_ids = Perfil.objects.filter(zona_usuario__icontains="juarez").values_list('usuario__pk', flat=True)
#                             solicitudes = Solicitud.objects.filter (usuario_id=usuarios_ids)
#                         else:
#                             solicitudes = Solicitud.objects.filter(usuario_id=self.request.user.pk)
#                             if g.name == "supervisor_laminacion":
#                                 usuarios_ids = Perfil.objects.filter(zona_usuario__icontains="laminacion").values_list('usuario__pk', flat=True)
#                                 solicitudes = Solicitud.objects.filter (usuario_id=usuarios_ids)
#                             else:
#                                 solicitudes = Solicitud.objects.filter(usuario_id=self.request.user.pk)
#                                 if g.name == "supervisor_centro":
#                                     usuarios_ids = Perfil.objects.filter(zona_usuario__icontains="centro").values_list('usuario__pk', flat=True)
#                                     solicitudes = Solicitud.objects.filter (usuario_id=usuarios_ids)
#                                 else:
#                                     solicitudes = Solicitud.objects.filter(usuario_id=self.request.user.pk)
#                                     if g.name == "supervisor_bajio":
#                                         usuarios_ids = Perfil.objects.filter(zona_usuario__icontains="bajio").values_list('usuario__pk', flat=True)
#                                         solicitudes = Solicitud.objects.filter (usuario_id=usuarios_ids)
#                                     else:
#                                         solicitudes = Solicitud.objects.filter(usuario_id=self.request.user.pk)
#                                         if g.name == "":
#                                             usuarios_ids = Perfil.objects.filter(zona_usuario__icontains="").values_list('usuario__pk', flat=True)
#                                             solicitudes = Solicitud.objects.filter (usuario_id=usuarios_ids)
#                                         else:
#                                             solicitudes = Solicitud.objects.filter(usuario_id=self.request.user.pk)
#         # for dato in solicitudes:
#         #     print (dato.usuario.id)
#         return solicitudes

class SolicitudesView(ListView):
    model = Solicitud
    template_name = 'gas/solicitudes.html'




class solicitar_create(CreateView):
    model = Solicitud
    fields = ['clave_proyecto', 'justificacion', 'gasolina_troca', 'gasolina_generador', 'total']
    success_url = reverse_lazy('gas:solicitudes')
    template_name = 'gas/solicitud.html'


    def form_valid(self, form):
        form.instance.usuario = self.request.user
        # print form.instance.usuario
        return super(solicitar_create, self).form_valid(form)


    def get_context_data(self, **kwargs):
        context = super(solicitar_create, self).get_context_data(**kwargs)
        context['tarjetas'] = Perfil.objects.filter(usuario_id=self.request.user.pk)
        context['sugerida'] = Vehiculo.objects.filter(usuario_id=self.request.user.pk)
        return context
 

class aprobar(UpdateView):
    model = Solicitud
    fields = ['total_aprobado', 'estatus']
    success_url = reverse_lazy('gas:solicitudes')
    template_name = 'gas/aprobar.html'


class listado_vehiculos(ListView):
    model = Vehiculo
    template_name = 'gas/vehiculos.html'

    def get_queryset(self):
        lista = Vehiculo.objects.filter(status='activo')
        return lista

def estatusVehiculo_request_object(request,vehiculo):
    obj = Vehiculo.objects.get(id = vehiculo)
    print obj.status
    if obj.status == 'Activo':
        obj.status = 'Inactivo'
    obj.save()
    return redirect('gas:listado_vehiculos')



class km_recorrer(ListView):
    model = Kilometraje
    template_name = 'gas/kilometraje.html'

    def get_queryset(self):
        kilometraje = Kilometraje.objects.filter(kilometraje=0)
        return kilometraje

    # def get_context_data(self, **kwargs):
    #     context = super(km_recorrer, self).get_context_data(**kwargs)

    #     context['sugerido'] = Kilometraje.objects.filter(vehiculo_id=self.vehiculo_id).aggregate(Avg('kilometraje'))
    #     return context

def nva_semana(request):
    obj = Vehiculo.objects.count()
    lista = Vehiculo.objects.filter(status="activo")
    for vehiculo in lista:
        nuevo = Kilometraje.objects.create(vehiculo_id=vehiculo.id,fecha=datetime.today(),kilometraje=0)
    print obj
    return redirect('gas:km_recorrer')


class update_kilometraje(UpdateView):
    model = Kilometraje
    fields = ['kilometraje']
    success_url = reverse_lazy('gas:km_recorrer')
    template_name = 'gas/agregar_kilometraje.html'


class horas_uso(ListView):
    model = HorasGeneradores
    template_name = 'gas/horas_uso.html'

    def get_queryset(self):
        horas = HorasGeneradores.objects.filter(horas_uso=0)
        return horas

class listado_generadores(ListView):
    model = Generadores
    template_name = 'gas/generadores.html'

    def get_queryset(self):
        lista = Generadores.objects.filter(estatus='Activo')
        return lista
        semana = HorasGeneradores.objects.filter(report_by_date__fecha=52)
        return semana

def nva_semana_generadores(request):
    obj = Generadores.objects.count()
    print obj
    lista = Generadores.objects.filter(estatus="Activo")
    print lista
    for generador in lista:
        nuevo = HorasGeneradores.objects.create(generador_id=generador.id,fecha=datetime.today(),horas_uso=0)
    print obj
    return redirect('gas:horas_uso')

class update_horas(UpdateView):
    model = HorasGeneradores
    fields = ['horas_uso']
    success_url = reverse_lazy('gas:horas_uso')
    template_name = 'gas/agregar_horas.html'

class agregar_generador(CreateView):
    model = Generadores
    fields = ['descripcion', 'obra', 'estatus', 'fecha_compra', 'serie', 'marca', 'marcador','estatus_obra', 'usuario']
    template_name = 'gas/agregar_generador.html'
    success_url = reverse_lazy('gas:horas_uso')



def estatus_request_object(request,generador):
    obj = Generadores.objects.get(id = generador)
    print obj.estatus
    if obj.estatus == 'Activo':
        obj.estatus = 'Inactivo'
    obj.save()
    return redirect('gas:listado_generadores')

class update_generadores(UpdateView):
    model = Generadores
    fields = ['descripcion', 'obra', 'estatus', 'fecha_compra', 'serie', 'marca', 'marcador','estatus_obra', 'usuario']
    success_url = reverse_lazy('gas:horas_uso')
    template_name = 'gas/update_generadores.html'

class update_vehiculos(UpdateView):
    model = Vehiculo
    fields = ['marca', 'modelo', 'zona', 'descripcion', 'placa', 'obra', 'anio', 'kilometraje_odometro', 'rendimiento', 'color', 'usuario', 'tipo', 'status']
    success_url = reverse_lazy('gas:listado_vehiculos')
    template_name = 'gas/update_vehiculos.html'

class saldos(ListView):
    model = Perfil
    template_name = 'gas/saldo.html'
    
    def get_queryset(self):
        filtro_tarjeta = Perfil.objects.filter(tiene_tarjeta=True)
        return filtro_tarjeta

class update_saldos(UpdateView):
    model = Perfil
    fields = ['saldo_tarjeta']
    success_url = reverse_lazy('gas:saldos')
    template_name = 'gas/update_saldos.html'

class reporte_generadores(DetailView):
    model = HorasGeneradores
    template_name = 'gas/reporte_generadores.html'
    queryset = HorasGeneradores.objects.all().order_by("-fecha")

    def get_context_data(self, **kwargs):
        context = super(reporte_generadores, self).get_context_data(**kwargs)
        context['reporte'] = HorasGeneradores.objects.filter(generador_id=self.kwargs['pk']).order_by("-fecha")
        print context
        return context

class reporte_vehiculos(DetailView):
    model = Kilometraje
    template_name = 'gas/reporte_vehiculos.html'

    def get_context_data(self, **kwargs):
        context = super(reporte_vehiculos, self).get_context_data(**kwargs)
        context['reporte'] = Kilometraje.objects.filter(vehiculo_id=self.kwargs['pk']).order_by("-fecha")
        return context