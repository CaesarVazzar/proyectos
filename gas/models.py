from django.db import models
from decimal import *
from django.contrib.auth.models import User
from django.utils import timezone
from django.forms.fields import DateField
from django.db.models import Avg, F
# Create your models here.

class Vehiculo(models.Model):

    CAMIONETA = 1
    AUTO = 2
    GENIE = 3
    GRUA = 4
    MANIPULADOR = 5
    TIPO = (
        (CAMIONETA, 'CAMIONETA'),
        (AUTO, 'AUTO'),
        (GENIE, 'GENIE'),
        (GRUA,'GRUA'),
        (MANIPULADOR,'MANIPULADOR'),
    )
    tipo = models.SmallIntegerField(choices=TIPO, verbose_name=('Tipo'), default=CAMIONETA)
    usuario = models.ForeignKey(User)
    marca = models.CharField(max_length=15,blank=True, null=True)
    modelo = models.CharField(max_length=30,blank=True, null=True)
    zona = models.CharField(max_length=15,blank=True, null=True)
    descripcion = models.CharField(max_length=100,blank=True, null=True)
    placa = models.CharField(max_length=15,blank=True, null=True)
    # residente = models.CharField(max_length=25,blank=True, null=True)
    obra = models.CharField(max_length=50,blank=True, null=True)
    anio = models.CharField(max_length=15,blank=True, null=True)
    kilometraje_odometro = models.IntegerField(blank=True, null=True)
    rendimiento = models.DecimalField(max_digits=5,decimal_places=2, blank=True, null=True)
    color = models.CharField(max_length=15,blank=True, null=True)
    status = models.CharField(max_length=20,blank=True,null=True)

    def __unicode__(self):
        return self.modelo

    def get_tipo_display(self):
        tipos = dict(self.TIPO)
        return tipos[self.tipo]

    @property
    def cantidad(self):
        kilometros_vehiculo = Kilometraje.objects.filter(vehiculo_id=self.id).order_by('-id')[:5].aggregate(Avg('kilometraje'))["kilometraje__avg"]
        val = (Decimal(kilometros_vehiculo)/self.rendimiento)* 18
        return val

    @property
    def cantidad_generadores(self):
        horas_prendido = Generadores.objects.filter(generadores_id=self.id).order_by('-id')[:5].aggregate(Avg('kilometraje'))["kilometraje__avg"]
        val = (Decimal(kilometros_vehiculo)/self.rendimiento)* 18
        return val
    

class Perfil(models.Model):
    usuario = models.ForeignKey(User)
    zona_usuario = models.CharField(max_length=15,blank=True,null=True)
    numero_tarjeta = models.CharField(max_length=30, blank=True, null=True)
    banco = models.CharField(max_length=30, blank=True, null=True)
    telefono = models.CharField(max_length=20,null=True)
    edad = models.CharField(max_length=3, null=True)
    is_supervisor = models.BooleanField(blank = True)
    tiene_tarjeta = models.BooleanField(blank=True, default=True)
    saldo_tarjeta = models.DecimalField(blank=True, max_digits=8, decimal_places=2, null=True)


    def __unicode__(self):
        return "%s %s" % (self.usuario.first_name,self.usuario.last_name)



class Solicitud(models.Model):

    SOLICITADO = 1
    REVISION = 2
    APROBADO = 3
    PAGADO = 4
    CANCELADO = 5
    ESTATUS = (
        (SOLICITADO, 'SOLICITADO'),
        (REVISION, 'REVISION'),
        (APROBADO, 'APROBADO'),
        (PAGADO,'PAGADO'),
        (CANCELADO,'CANCELADO'),
    )
    estatus = models.SmallIntegerField(choices=ESTATUS, verbose_name=('Estatus'), default=SOLICITADO)
    usuario = models.ForeignKey(User)
    clave_proyecto = models.CharField(max_length=200,blank=True, null=True)
    justificacion = models.TextField(verbose_name='Justificacion')
    gasolina_troca = models.DecimalField(max_digits=8,decimal_places=2,verbose_name='Gasolina troca', blank=True, null=True)
    gasolina_generador = models.DecimalField(max_digits=8,decimal_places=2,verbose_name='Gasolina generador', blank=True, null=True)
    total = models.DecimalField(max_digits=8,decimal_places=2,verbose_name='Total Solicitado', blank=True, null=True)
    total_aprobado = models.DecimalField(max_digits=8,decimal_places=2,verbose_name='Total Aprobado', blank=True, null=True)
    fecha_solicitud = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    fecha_revision = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    fecha_aprobacion = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    fecha_pago = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    fecha_cancelacion = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    class Meta:
        permissions = (
            ("can_review", "Can review"),
            ("can_approve", "Can approve"),
            ("can_pay", "Can pay"),
            ("can_cancel", "Can cancel"),
            ("can_kilometros", "Can kilometros"),
            ("can_generadores", "Can generadores"),
            ("can_saldos", "Can saldos"),
            )

    def __unicode__(self):
        return '%s' % (self.usuario_id)

    def get_tipo_display(self):
        status = dict(self.ESTATUS)
        return status[self.estatus]

    @property
    def cantidad_sugerida(self):
        usuario_cantidad = Solicitud.objects.filter(usuario=self.usuario).aggregate(Avg('total'))
        return usuario_cantidad

    

class Kilometraje(models.Model):
    vehiculo = models.ForeignKey(Vehiculo)
    kilometraje = models.DecimalField(max_digits=10, decimal_places=0, null=True, blank=True)
    fecha = models.DateField(blank=True, null=True)
    activo = models.BooleanField(blank=True, default=True)
    # @property
    # def promedio(self):
    #     promedio_kilometraje = Kilometraje.objects.values(
    #         'vehiculo_id',
    #         'kilometraje').aggregate(
    #         vehiculo_id=F('vehiculo_id'),
    #         kilometraje=Avg('kilometraje')
    #         )
    #     print promedio_kilometraje


        # gasolina = 18
        # promedio_kilometraje = Kilometraje.objects.filter(vehiculo_id=self.vehiculo_id).aggregate(Avg('kilometraje'))
        # # promedio_kilometraje = Kilometraje.objects.filter(vehiculo_id=self.vehiculo_id)
        # # promedio_kilometraje = Kilometraje.objects.filter(kilometraje=gasolina)
        # print promedio_kilometraje
        # return promedio_kilometraje

    # @property
    # def promedio(self):
        # c = c.annotate(metric=Sum('results__metric'))
        # c = c.annotate(metric_prior=Sum('results__metric_prior'))
        # c = c.annotate(variance=F('metric')-F('metric_prior'))


class Generadores(models.Model):
    descripcion = models.CharField(max_length=50, blank=True, null=True)
    obra = models.CharField(max_length=200,blank=True, null=True)
    estatus = models.CharField(max_length=20,blank=True,null=True)
    fecha_compra = models.DateField(blank=True, null=True)
    serie = models.CharField(max_length=30, blank=True, null=True)
    marca = models.CharField(max_length=30, blank=True, null=True)
    marcador = models.CharField(max_length=15, blank=True, null=True)
    estatus_obra = models.CharField(max_length=30, blank=True, null=True)
    usuario = models.ForeignKey(User)

    def __unicode__(self):
        return self.descripcion

class HorasGeneradores(models.Model):
    generador = models.ForeignKey(Generadores)
    horas_uso = models.IntegerField(blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True)