from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from gas.models import Solicitud, Vehiculo, Kilometraje, Generadores, Perfil
from django.forms import ModelForm, TextInput
from datetime import datetime
from django.contrib.admin.widgets import AdminDateWidget 
from django.forms.fields import DateField
from django.forms.widgets import Widget


class NvaSemanaForm(forms.Form):
    vehiculo = forms.CharField(required=True)
    kilometraje = forms.CharField(required=True)
    fecha = forms.CharField()


class SolicitudForm(forms.ModelForm):
    usuario = forms.CharField(required=True)
    clave_proyecto = forms.CharField(max_length=200,required=True)
    justificacion = forms.CharField()
    gasolina_troca = forms.DecimalField(max_digits=8,decimal_places=2, required=True)
    gasolina_generador = forms.DecimalField(max_digits=8,decimal_places=2, required=True)
    total = forms.DecimalField(widget=forms.TextInput(attrs={'readonly':'readonly'}), max_digits=8,decimal_places=2, required=True)
    estatus = forms.IntegerField()



class VehiculoForm(forms.ModelForm):
    class Meta:
        model = Vehiculo
        fields = ['marca', 'modelo', 'zona', 'descripcion', 'placa', 'obra', 'anio', 'kilometraje_odometro', 'rendimiento', 'color', 'usuario', 'tipo', 'status']


class KilometrajeForm(forms.ModelForm):
    class Meta:
        model = Kilometraje
        fields = ["kilometraje"]
        widgets = {
            'kilometraje': forms.TextInput(attrs={'class': 'form-control'}),
        }

class GeneradoresForm(forms.ModelForm):
    class Meta:
        model = Generadores
        fields = ['descripcion', 'obra', 'estatus', 'fecha_compra', 'serie', 'marca','marcador','estatus_obra','usuario']


class PerfilForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = ['saldo_tarjeta']