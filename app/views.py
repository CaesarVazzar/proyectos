from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse,HttpResponseRedirect, HttpRequest
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext, Template
from django.core.mail import EmailMessage   
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from datetime import datetime


def home(request):
    return render(request, "cnycn/index.html", {})